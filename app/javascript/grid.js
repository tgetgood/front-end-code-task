'use strict';

const COLOURS = ['red', 'green', 'blue', 'yellow'];
const MAX_X = 10;
const MAX_Y = 10;

class Block {
  constructor(x, y) {
    this.x = x;
    this.y = y;
    this.colour = COLOURS[Math.floor(Math.random() * COLOURS.length)];
    this.clickable = true;
  }
}

class BlockGrid {
  constructor() {
    this.grid = [];

    for (let x = 0; x < MAX_X; x++) {
      const col = [];
      for (let y = 0; y < MAX_Y; y++) {
        col.push(new Block(x, y));
      }

      this.grid.push(col);
    }

    return this;
  }

  redrawBlock(blockEl, block) {
    const { x, y, colour } = block;
    const id = `block_${x}x${y}`;

    blockEl.id = id;
    blockEl.className = 'block';
    blockEl.style.background = block.colour;
  }

  isSameColourNeighbour(block, x, y) {
    if (x > MAX_X - 1 || y > MAX_Y - 1 || x < 0 || y < 0) return false;

    return (this.grid[x][y].clickable && this.grid[x][y].colour === block.colour);
  }

  getMatchingNeighbours(block) {
    const newMatches = [];

    if (this.isSameColourNeighbour(block, block.x + 1, block.y)) {
      newMatches.push(this.grid[block.x + 1][block.y]);
    }

    if (this.isSameColourNeighbour(block, block.x - 1, block.y)) {
      newMatches.push(this.grid[block.x - 1][block.y]);
    }

    if (this.isSameColourNeighbour(block, block.x, block.y + 1)) {
      newMatches.push(this.grid[block.x][block.y + 1]);
    }

    if (this.isSameColourNeighbour(block, block.x, block.y - 1)) {
      newMatches.push(this.grid[block.x][block.y - 1]);
    }

    return newMatches;
  }

  resolveNeighbours(block, matched = []) {
    const newMatches = this.getMatchingNeighbours(block);

    if (newMatches.length > 0) {
      newMatches.forEach((removedBlock) => {
        removedBlock.clickable = false;
        this.resolveNeighbours(removedBlock, [...matched, ...newMatches]);
      });
    }
  }

  render(grid = document.querySelector('#gridEl')) {
    let el = grid.cloneNode(false);
    grid.parentNode.replaceChild(el, grid);
    for (let x = 0; x < MAX_X; x++) {
      const id = 'col_' + x;
      const colEl = document.createElement('div');
      colEl.className = 'col';
      colEl.id = id;
      el.appendChild(colEl);

      for (let y = MAX_Y - 1; y >= 0; y--) {
        const block = this.grid[x][y];
        const blockEl = document.createElement('div');

        if (block.clickable) {
          blockEl.addEventListener('click', (evt) => this.blockClicked(evt, block));
        }

        colEl.appendChild(blockEl);
        this.redrawBlock(blockEl, block);
      }
    }

    return this;
  }

  removeBlocks() {
    const collapseCols = [];
    for (let x = 0; x < MAX_X; x++) {
      for (let y = 0; y < MAX_Y; y++) {
        if (!this.grid[x][y].clickable) {
          this.grid[x][y].colour = 'black';

          collapseCols[x] = true;
        }
      }
    }

    return collapseCols;
  }

  findNextBlock(x, start) {
    if (start < MAX_Y) {
      for (let y = start; y < MAX_Y; y++) {
        if (this.grid[x][y].clickable === true) {
          return y;
        }
      }
    }

    return -1;
  }

  collapseColumns() {
    const colsToCollapse = this.removeBlocks();

    colsToCollapse.forEach((val, key) => {
      for (let y = 0; y < MAX_Y; y++) {
        if (this.grid[key][y].clickable === false) {
          let replaceWith = this.findNextBlock(key, y + 1);
          if (replaceWith > 0) {
            this.grid[key][y].colour = this.grid[key][replaceWith].colour;
            this.grid[key][y].clickable = true;

            this.grid[key][replaceWith].colour = 'black';
            this.grid[key][replaceWith].clickable = false;
          }
        }
      }
    });
  }

  checkGameOver() {
    for (let x = 0; x < MAX_X; x++) {
      for (let y = 0; y < MAX_Y; y++) {
        const validBlock = this.grid[x][y].clickable ? this.grid[x][y] : undefined;
        if (validBlock && this.getMatchingNeighbours(validBlock).length > 0) {
          return false;
        }
      }
    }

    return true;
  }

  calculateScore() {
    let remainingBlocks = 0;
    for (let x = 0; x < MAX_X; x++) {
      for (let y = 0; y < MAX_Y; y++) {
        if (this.grid[x][y].clickable) {
          remainingBlocks++;
        }
      }
    }
    return remainingBlocks;
  }

  blockClicked(e, block) {
    this.resolveNeighbours(block);
    this.collapseColumns();

    this.render();

    if (this.checkGameOver()) {
      const totalBlocks = MAX_X * MAX_Y;
      const gameOverEl = document.createElement('div');
      gameOverEl.id = 'game-over';
      gameOverEl.innerHTML = `<h1>Game over!</h1><h2>You scored ${totalBlocks - this.calculateScore()} out of ${totalBlocks}</h2>`;
      document.querySelector('#gridEl').appendChild(gameOverEl);
    }
  }
}

window.addEventListener('DOMContentLoaded', () => new BlockGrid().render());
