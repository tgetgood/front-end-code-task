const { expect } = chai;
describe("BlockGrid", function() {
  "use strict";
  const blockGrid = new BlockGrid();

  function clearGrid() {
    const { grid } = blockGrid;
    for (let x = 0; x < MAX_X; x++) {
      for (let y = 0; y < MAX_Y; y++) {
        grid[x][y].colour = 'black';
        grid[x][y].clickable = false;
      }
    }
  }

  function activateBlock(block, colour) {
    block.colour = colour;
    block.clickable = true;
  }

  describe('getNeighbours', () => {
    this.beforeEach(() => {
      clearGrid();
    });

    it('returns nothing for a removed block', () => {
      const { grid } = blockGrid;
      activateBlock(grid[3][3], 'red');
      activateBlock(grid[3][4], 'red');
      activateBlock(grid[2][4], 'red');
      activateBlock(grid[4][4], 'red');

      expect(blockGrid.getMatchingNeighbours(grid[0][4]).length).to.eql(0);
    });

    it('returns the matching neighbours to either side and above', () => {
      const { grid } = blockGrid;
      activateBlock(grid[3][3], 'red');
      activateBlock(grid[3][4], 'red');
      activateBlock(grid[2][4], 'red');
      activateBlock(grid[4][4], 'red');

      expect(blockGrid.getMatchingNeighbours(grid[3][4]).length).to.eql(3);
    });

    it('returns correct results for corner blocks', () => {
      const { grid } = blockGrid;
      activateBlock(grid[0][0], 'red');
      activateBlock(grid[0][1], 'red');
      activateBlock(grid[1][0], 'red');
      activateBlock(grid[1][1], 'red');

      expect(blockGrid.getMatchingNeighbours(grid[0][0]).length).to.eql(2);
    });

    it('returns correct results for blocks on the edges', () => {
      const { grid } = blockGrid;
      activateBlock(grid[4][0], 'red');
      activateBlock(grid[5][0], 'red');
      activateBlock(grid[6][0], 'red');

      expect(blockGrid.getMatchingNeighbours(grid[5][0]).length).to.eql(2);
    });
  });

  describe('resolveNeighbours', () => {
    this.beforeEach(() => {
      clearGrid();
    });

    it('marks the matching neighbours for removal (not clickable)', () => {
      const { grid } = blockGrid;
      activateBlock(grid[3][3], 'red');
      activateBlock(grid[3][4], 'red');
      activateBlock(grid[2][4], 'red');
      activateBlock(grid[4][4], 'red');
      activateBlock(grid[4][5], 'green');
      activateBlock(grid[3][5], 'yellow');

      blockGrid.resolveNeighbours(grid[3][4]);

      let activeBlocks = 0;
      for (let x = 0; x < MAX_X; x++) {
        for (let y = 0; y < MAX_Y; y++) {
          if (grid[x][y].clickable) {
            activeBlocks ++;
          }
        }
      }

      expect(activeBlocks).to.eql(2);
    });
  });

  describe('findNextBlock', () => {
    this.beforeEach(() => {
      clearGrid();
    });

    it('should return the y index of the next active block in a column', () => {
      const { grid } = blockGrid;
      activateBlock(grid[3][0], 'red');
      activateBlock(grid[3][3], 'green');
      activateBlock(grid[3][7], 'blue');
      activateBlock(grid[3][8], 'yellow');

      expect(blockGrid.findNextBlock(3, 4)).to.eql(7);
    });

    it('should return -1 if there are no more active blocks in a column', () => {
      const { grid } = blockGrid;
      activateBlock(grid[3][0], 'red');
      activateBlock(grid[3][3], 'green');

      expect(blockGrid.findNextBlock(3, 4)).to.eql(-1);
    });
  });

  describe('removeBlocks', () => {
    this.beforeEach(() => {
      clearGrid();
    });

    it('turns the removable blocks black', () => {
      const { grid } = blockGrid;
      activateBlock(grid[3][3], 'red');
      activateBlock(grid[3][4], 'red');
      activateBlock(grid[2][4], 'red');
      activateBlock(grid[4][4], 'red');
      activateBlock(grid[4][5], 'green');
      activateBlock(grid[3][5], 'yellow');

      blockGrid.resolveNeighbours(grid[3][4]);

      blockGrid.removeBlocks();
      expect(grid[3][3].colour).to.eql('black');
    });

    it('you cannot remove a single block', () => {
      const { grid } = blockGrid;
      activateBlock(grid[3][3], 'red');

      blockGrid.resolveNeighbours(grid[3][3]);

      blockGrid.removeBlocks();
      expect(grid[3][3].colour).to.eql('red');
    });
  });

  describe('collapseColumns', () => {
    this.beforeEach(() => {
      clearGrid();
    });

    it('should move all active blocks to the bottom of the column', () => {
      const { grid } = blockGrid;
      activateBlock(grid[3][0], 'red');
      activateBlock(grid[3][1], 'green');
      activateBlock(grid[3][7], 'blue');
      activateBlock(grid[3][8], 'yellow');

      blockGrid.collapseColumns();

      expect(grid[3][0].colour).to.eql('red');
      expect(grid[3][1].colour).to.eql('green');
      expect(grid[3][2].colour).to.eql('blue');
      expect(grid[3][3].colour).to.eql('yellow');
      expect(grid[3][4].colour).to.eql('black');
      expect(grid[3][5].colour).to.eql('black');
      expect(grid[3][6].colour).to.eql('black');
      expect(grid[3][7].colour).to.eql('black');
      expect(grid[3][8].colour).to.eql('black');
      expect(grid[3][9].colour).to.eql('black');
    });
  });

  describe('checkGameOver', () => {
    this.beforeEach(() => {
      clearGrid();
    });

    it('should return true if there are no possible moves left', () => {
      const { grid } = blockGrid;
      activateBlock(grid[3][3], 'red');
      activateBlock(grid[3][4], 'green');
      activateBlock(grid[2][4], 'blue');
      activateBlock(grid[4][4], 'yellow');

      expect(blockGrid.checkGameOver()).to.eql(true);
    });

    it('should return false if there are still possible moves', () => {
      const { grid } = blockGrid;
      activateBlock(grid[3][3], 'red');
      activateBlock(grid[3][4], 'red');
      activateBlock(grid[2][4], 'red');
      activateBlock(grid[4][4], 'red');

      expect(blockGrid.checkGameOver()).to.eql(false);
    });

  });

  describe('calculateScore', () => {
    this.beforeEach(() => {
      clearGrid();
    });

    it('should return correct score if blocks are remaining', () => {
      const { grid } = blockGrid;
      activateBlock(grid[7][7], 'blue');
      activateBlock(grid[0][9], 'green');

      expect(blockGrid.calculateScore()).to.eql(2);
    });

    it('should return correct score (zero) if no blocks are remaining', () => {
      expect(blockGrid.calculateScore()).to.eql(0);
    });
  });
});
